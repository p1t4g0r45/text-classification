#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, division

import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import fetch_20newsgroups_vectorized
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.datasets import fetch_20newsgroups
from sklearn.datasets import fetch_california_housing
from sklearn.datasets import load_digits
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn.decomposition import PCA, NMF, TruncatedSVD
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.decomposition import TruncatedSVD
from sklearn.random_projection import sparse_random_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.sparse import csr_matrix
from tempfile import mkdtemp
from sklearn.externals.joblib import Memory

print(__doc__)

cachedir = mkdtemp()
memory = Memory(cachedir=cachedir, verbose=10)

pipe = Pipeline([
    ('reduce_dim', NMF()),
    ('classify', LinearSVC())
], memory=memory)

N_FEATURES_OPTIONS = [128]
C_OPTIONS = [1, 10, 100, 1000]
param_grid = [
    {
        'reduce_dim': [TruncatedSVD()],
        'reduce_dim__n_components': N_FEATURES_OPTIONS,
        'classify__C': C_OPTIONS
    },
    {
        'reduce_dim': [NMF()],
        'reduce_dim__n_components': N_FEATURES_OPTIONS,
        'classify__C': C_OPTIONS
    },
    {
        'reduce_dim': [SelectKBest(chi2)],
        'reduce_dim__k': N_FEATURES_OPTIONS,
        'classify__C': C_OPTIONS
    },
]
reducer_labels = ['TruncatedSVD', 'NMF', 'KBest(chi2)']

cats = ['alt.atheism', 'sci.space', 'rec.autos', 'sci.med', 'comp.graphics', 'rec.sport.baseball']

grid = GridSearchCV(pipe, cv=3, n_jobs=4, param_grid=param_grid)
newsgroups = fetch_20newsgroups(subset='test')#, categories=cats)

vectorizer = TfidfVectorizer()
vectors = vectorizer.fit_transform(newsgroups.data)
grid.fit(csr_matrix(vectors), newsgroups.target)

mean_scores = np.array(grid.cv_results_['mean_test_score'])
# scores are in the order of param_grid iteration, which is alphabetical
mean_scores = mean_scores.reshape(len(C_OPTIONS), -1, len(N_FEATURES_OPTIONS))
# select score for best C
mean_scores = mean_scores.max(axis=0)
bar_offsets = (np.arange(len(N_FEATURES_OPTIONS)) *
               (len(reducer_labels) + 1) + .5)

plt.figure()
COLORS = 'bgrcmyk'
for i, (label, reducer_scores) in enumerate(zip(reducer_labels, mean_scores)):
    plt.bar(bar_offsets + i, reducer_scores, label=label, color=COLORS[i])

plt.title("Comparing feature reduction techniques")
plt.xlabel('Reduced number of features')
plt.xticks(bar_offsets + len(reducer_labels) / 2, N_FEATURES_OPTIONS)
plt.ylabel('News groups classification accuracy')
plt.ylim((0, 1))
plt.legend(loc='upper left')

plt.show()
